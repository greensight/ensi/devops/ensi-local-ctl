# DEPRECATED
# Используйте [ELC](https://github.com/ensi-platform/elc) и [elc-workspace](https://github.com/ensi-platform/elc-workspace)


# ELC - Ensi Local Ctl #

ELC - набор скриптов для запуска микросервисов и инфраструктурных сервисов в локальном окружении для разработки.

[Описание](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/362676232/Backend-)

### Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
