### General
## Название сервиса, по умолчанию название папки
# PROJECT_NAME=
## Префикс контейнеров, по умолчанию `<global COMPOSE_PROJECT_NAME>-<PROJECT_NAME>`
# COMPOSE_PROJECT_NAME=

### Containers
## Базовый образ PHP, на основе которого собираются остальные образы
# ELC_BASE_APP_IMAGE=

## Название базового образа приложения
ELC_APP_IMAGE=ensi/app-dev:latest
## путь до докерфайла для сборки образа app
ELC_APP_DOCKERFILE=${ELC_APP_DOCKERFILE}
## Путь до entrypoint для PHP контейнеров
ELC_APP_ENTRYPOINT=${ELC_APP_ENTRYPOINT}

## Название образа nginx
# ELC_NGINX_IMAGE=
## Путь до файла nginx/conf.d/default.conf
ELC_NGINX_CONF=${ELC_NGINX_CONF}
## DOCUMENT_ROOT внутри контейнера nginx
ELC_DOCUMENT_ROOT=/var/www/public

## Домен сервиса
# ELC_APP_HOST=service.ensi.127.0.0.1.nip.io

### Network
## Название docker network
ELC_NETWORK=${ELC_NETWORK}

## Папка с кодом сервиса
# ELC_PROJECT_DIR=/path/to/service/repository/root
## Папка с dev пакетам
ELC_DEV_PACKAGES_DIR=${ELC_DEV_PACKAGES_DIR}

### Tools
##
ELC_SHRC=${ELC_SHRC}

## Путь до папки с настройками composer
ELC_COMPOSER_HOME=${ELC_COMPOSER_HOME}
## Путь до папки с кешем composer
ELC_COMPOSER_CACHE_DIR=${ELC_COMPOSER_CACHE_DIR}
## путь до папки с кешем npm
NPM_CACHE_DIR=${NPM_CACHE_DIR}
## Путь до XDG_CONFIG_DIR
CONFIG_DIR=${CONFIG_DIR}
