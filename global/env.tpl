COMPOSE_PROJECT_NAME=ensi

## Базовый домен для всех микросервисов
ELC_BASE_DOMAIN=.ensi.127.0.0.1.nip.io

### Volumes
ELC_APPS_DIR=${ELC_APPS_DIR}
# ELC_APP_SUBDIR=source/
ELC_DATA_DIR=${ELC_DATA_DIR}
ELC_CONFIG_DIR=${ELC_CONFIG_DIR}
# Путь до сервиса в котором выполнять команды для пакетов. По умолчанию испоьзуется сервис customers/crm. Важно указать / в конце.
# ELC_DEV_PACKAGES_EXEC_DIR=/path/to/apps/service/

### Proxy
ELC_PROXY_PORT=80

### Postgres
ELC_POSTGRES_PORT=5432
ELC_POSTGRES_PASSWORD=example

### Kafka
ELC_KAFKA_PORT=9092
# ELC_KAFKA_UI_HOST=kafka-ui.ensi.127.0.0.1.nip.io

### Elasticsearch
ELC_ELASTIC_PORT=9200
# ELC_KIBANA_HOST=kibana.ensi.127.0.0.1.nip.io

### Redis
ELC_REDIS_PORT=6379
# ELC_REDIS_UI_HOST=redis.ensi.127.0.0.1.nip.io

### ES
# ELC_ES_HOST=es.ensi.127.0.0.1.nip.io
# ELC_ES_IMGPROXY_HOST=imgproxy.ensi.127.0.0.1.nip.io