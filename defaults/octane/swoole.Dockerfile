ARG BASE_IMAGE

FROM composer as composer

FROM $BASE_IMAGE

ARG GROUP_ID

RUN grep :$GROUP_ID: /etc/group | awk 'BEGIN {FS=":"}; {print $1}' | xargs -r delgroup
RUN addgroup --gid $GROUP_ID user
RUN addgroup www-data user

RUN apk add npm git openjdk8-jre-base bash htop

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN apk add --virtual .build-deps --no-cache --update autoconf file g++ gcc libc-dev make pkgconf re2c zlib-dev && \
    pecl install xdebug swoole && \
    docker-php-ext-enable xdebug swoole && \
    apk del .build-deps

CMD ["php", "artisan", "octane:start"]